document.getElementById("copy").addEventListener("click", copyFunc);

function copyFunc() {
    const text = document.getElementById("numcopy");

    text.select();
    text.setSelectionRange(0, 99999);

    document.execCommand("copy");

    alert("Copied the text: " + text.innerHTML);
    console.log(text);
}